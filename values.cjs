let values = (testObject) => {
  let result = [];
  for (const key in testObject) {
    result.push(`${testObject[key]}`);
  }
  return result;
};

module.exports = values;
