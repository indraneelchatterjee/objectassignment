let mapObject = (testObject) => {
  let result = {};
  for (const key in testObject) {
    result[key] = testObject[key] + 4;
  }
  return result;
};

module.exports = mapObject;
