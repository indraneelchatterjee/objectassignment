let pairs = (testObject) => {
  let result = [];
  for (const key in testObject) {
    result.push([key, testObject[key]]);
  }
  return result;
};

module.exports = pairs;
