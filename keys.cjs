let keys = (testObject) => {
  let result = [];
  for (const key in testObject) {
    result.push(`${key}`);
  }
  return result;
};

module.exports = keys;
