let invert = (testObject) => {
  let result = {};
  for (let key in testObject) {
    result[testObject[key]] = key;
  }
  return result;
};

module.exports = invert;
