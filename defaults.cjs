let defaults = (testObject, defaultProps) => {
  for (const key in defaultProps) {
    if (defaultProps[key] !== testObject[key])
      testObject[key] = defaultProps[key];
  }
  return testObject;
};

module.exports = defaults;
